/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#include "CheckerboardCornerClassifier.h"
#include "CalibOptimization.h"

void test_CheckerboardCornerClassifier(){
    CheckerboardCornerClassifier ccc;
    cv::Mat ker,ker_inv;
    ccc.genKer1(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer1(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    ccc.genKer2(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer2(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    ccc.genKer3(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer3(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    ccc.genKer4(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer4(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    ccc.genKer5(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer5(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    ccc.genKer6(ker,5,false);
    std::cout << ker << std::endl;
    ccc.genKer6(ker_inv,5,true);
    std::cout << ker_inv << std::endl;
    
    std::cout << ker.mul(ker_inv) << std::endl;
    
    cv::Mat gray = cv::imread("/media/PROJ/09-Tools/CornerLineDetector/000031.png", cv::IMREAD_GRAYSCALE);
    
    std::vector<cv::Point2f> cur_corners;
    cv::goodFeaturesToTrack(gray, cur_corners, 5000, 0.02, 10, cv::noArray(), 5);
    cv::cornerSubPix(gray, cur_corners,
                     cv::Size(7,7),
                     cv::Size(-1,-1),
                     cv::TermCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS, 20, 0.01));
    
    std::vector<cv::Point2f> cb_corners;
    ccc.apply(gray,cur_corners, cb_corners);
    
    cv::Mat toshow;
    cv::cvtColor(gray,toshow,CV_GRAY2BGR);
    
    for( size_t i = 0; i < cb_corners.size(); i++){
        cv::circle(toshow, cb_corners[i], 3, cv::Scalar(0,0,255),2,CV_AA);
    }
    cv::imshow("img",toshow);
    cv::waitKey(-1);
}

void test_caliboptimization(){
    CalibOptimization cal_op;
    double M[4][3] = {{0,0,0},{0,1,0},{1,1,0},{1,0,0}};
    double camera[3][7] = {{1,0,0,0,1,0,-0.5},
                         {0.7133,0.1100,0.3141,0.6168,0,0.5,-0.5},
                         {0.7110,0.3605,0.5945,0.1054,0.5,0,-0.5}};
    
    double kvec[4] = {257,257,320,240};
    double dvec[1] = {2.0};
                     
    
    std::vector<std::vector<cv::Point2f> > vec_imgpts;
    std::vector<std::vector<cv::Point3f> > vec_objpts;
    std::vector<double> vec_poses;
    
    vec_imgpts.resize(3);
    vec_objpts.resize(3);
    vec_poses.resize(7*3);
    
    for( size_t k = 0; k < 3; k++){
        for( size_t i = 0; i < 4; i++){
            double rx,ry;
            projectFOV(kvec,dvec,camera[k],M[i],rx,ry);
            vec_imgpts[k].push_back(cv::Point2f(rx,ry));
            vec_objpts[k].push_back(cv::Point3f(M[i][0],M[i][1],M[i][2]));
            std::copy(camera[k],camera[k]+7, vec_poses.begin()+7*k);
        }
    }    
    
    kvec[0]+=5;
    dvec[1] = 0.5;
    cal_op.apply(kvec, dvec, vec_objpts, vec_imgpts, vec_poses);
    for(size_t i = 0; i < cal_op._kvec.size(); i++)
        std::cout << cal_op._kvec[i] << " ";
    std::cout << std::endl;
}

int main(int argc, char** argv){
    //test_CheckerboardCornerClassifier();
    test_caliboptimization();
    return 0;
}
