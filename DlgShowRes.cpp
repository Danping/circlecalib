#include "DlgShowRes.h"
#include "ui_DlgShowRes.h"
#include "CircleCalib.h"

#include <iomanip>
#include <sstream>

DlgShowRes::DlgShowRes(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgShowRes)
{
    ui->setupUi(this);
    
    
    stringstream ss;
    ss << "#intrinsic matrix:" << std::endl;
    for( int i = 0; i < 3; i++){
        ss << setprecision(12)
           <<  (getData().K.at<double>(i,0)) << " " 
           <<  (getData().K.at<double>(i,1)) << " " 
           <<  getData().K.at<double>(i,2) << std::endl;
    }
    
    ss << "#distortion:" << std::endl;
    ss << setprecision(12) 
       << getData().dc.at<double>(0,0) <<  " "
       << getData().dc.at<double>(0,1) <<  " "
       << getData().dc.at<double>(0,2) <<  " "
       << getData().dc.at<double>(0,3);    
    if(getData().dc.cols == 5){
       ss << " " << getData().dc.at<double>(0,4);
    }
    ss <<  std::endl;
    
    ss << "#image size(h w) :" << std::endl;
    ss << getData()._current_image.height() << " " << getData()._current_image.width();
    
    ui->textEdit->setText(QString(ss.str().c_str()));
}

DlgShowRes::~DlgShowRes()
{
    delete ui;
}
