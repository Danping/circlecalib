#include "RenderArea.h"
#include "CircleCalib.h"
#include <QPainter>
#include <QDebug>

RenderArea::RenderArea(QWidget *parent) :
    QWidget(parent)
{
    
}

inline void img2screen(QRect rcDraw, int w, int h, float& x, float& y){
    float xs = rcDraw.left() + rcDraw.width()*x/w;
    float ys = rcDraw.top() + rcDraw.height()*y/h;
    
    x = xs;
    y = ys;    
}
inline void screen2img(QRect rcDraw, int w, int h, float& x, float& y){
    float xi = w*(x - rcDraw.left())*1.0f/rcDraw.width();
    float yi = h*(y - rcDraw.top())*1.0f/rcDraw.height();
    
    x = xi;
    y = yi;
}

QRect RenderArea::getDrawRect(){
    
    float imgRatio = 1.0f;
    QImage& img = getData()._current_image;
    
    int iw = img.width();
    int ih = img.height();
    
    int x1,y1,nw, nh;
    
    if( width()*ih > height()*iw){
        nh = height()-6;
        imgRatio = float(iw)/float(ih);
        nw = (int)(imgRatio*nh +0.5);
    }else{
        nw = width()-6;
        imgRatio = float(ih)/float(iw);
        nh = (int)(imgRatio*nw+0.5);
    }

    int cx = width()/2;
    int cy = height()/2;

    x1 = cx - nw/2;
    y1 = cy - nh/2;        

    return QRect(x1,y1,nw, nh);
    return QRect();
}

int _color[4][3] = {{255,0,0},{255,255,0},{0,255,0},{0,0,255}};

void RenderArea::paintEvent(QPaintEvent *){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    //painter.fillRect(QRect(0,0,this->width(),this->height()),QColor(50,50,100));
    
    if( getData()._current_image.isNull())
        return;
    
    QImage img = getData()._current_image;      
    QRect rcDraw = getDrawRect();    
        
    QPixmap pm;
    pm.convertFromImage(img);
    painter.drawPixmap(rcDraw,pm);
    
    if( !getData()._current_cb_corners.empty()){
        //draw detected blobs
        painter.setPen(QPen(QColor(255,0,0)));
        if( getData()._bound_corner_ids.size() != 4 || getData()._cb_corner_order.empty()){
            for(size_t i = 0; i < getData()._current_cb_corners.size(); i++){
                float x = getData()._current_cb_corners[i].x;
                float y = getData()._current_cb_corners[i].y;
                
                img2screen(rcDraw, img.width(), img.height(), x,y);
                painter.drawLine(x-3,y,x+3,y);
                painter.drawLine(x,y-3,x,y+3);
            }
        }
        
//        //draw transformed points
//        if( getData().homoTransPts.size()){
//            painter.setPen(QPen(QColor(255,255,0)));
//            for(size_t i = 0; i < getData().homoTransPts.size(); i++){
//                float x = getData().homoTransPts[i].x;
//                float y = getData().homoTransPts[i].y;
                
//                img2screen(rcDraw, img.width(), img.height(), x,y);
//                painter.drawLine(x-3,y,x+3,y);
//                painter.drawLine(x,y-3,x,y+3);
//            }
//        }
        
        
        for( size_t k = 0; k < getData()._bound_corner_ids.size(); k++){
            int i = getData()._bound_corner_ids[k];
            
            float x = getData()._current_cb_corners[i].x;
            float y = getData()._current_cb_corners[i].y;
            
            painter.setPen(QPen(QColor(_color[k][0],_color[k][1],_color[k][2])));
            
            img2screen(rcDraw, img.width(), img.height(), x,y);
            painter.drawEllipse(x-10,y-10,21, 21);
        }
        
        if( getData()._bound_corner_ids.size() == 4 && getData()._cb_corner_order.size() > 0){
            int nColor = getData().MGRIDS*getData().NGRIDS;
            float dc = 360.0f/(nColor-1);
            
            for( int k = 0; k < nColor; k++){
                float h = std::min(dc*k,359.0f);
                QColor clr = QColor::fromHsl((int)h,255,125);
                painter.setPen(QPen(clr));
                
                int i = getData()._cb_corner_order[k];
                if( i >= 0){                    
                    float x = getData()._current_cb_corners[i].x;
                    float y = getData()._current_cb_corners[i].y;
                    img2screen(rcDraw, img.width(), img.height(), x,y);
                    painter.drawEllipse(x-5,y-5,11,11);
                    painter.drawLine(x-3,y,x+3,y);
                    painter.drawLine(x,y-3,x,y+3);   
                }
            }
            
            //draw lines to connect them
            for( int k = 1; k < nColor; k++){
                float h = std::min(dc*k,359.0f);
                QColor clr = QColor::fromHsl((int)h,255,125);
                painter.setPen(QPen(clr));
                
                int i0 = getData()._cb_corner_order[k-1];
                int i1 = getData()._cb_corner_order[k];
                
                if( i0 >= 0 && i1 >= 0){       
                    float x0 = getData()._current_cb_corners[i0].x;
                    float y0 = getData()._current_cb_corners[i0].y;
                    
                    float x1 = getData()._current_cb_corners[i1].x;
                    float y1 = getData()._current_cb_corners[i1].y;
                    img2screen(rcDraw, img.width(), img.height(), x0, y0);
                    img2screen(rcDraw, img.width(), img.height(), x1, y1);
                    
                    painter.drawLine(x0, y0, x1, y1);
                }
            }
        }
    }
    
}

void RenderArea::mousePressEvent(QMouseEvent* event){
    if( getData()._current_cb_corners.size() != 0){
        if( event->buttons() & Qt::LeftButton ) {
            QRect rcDraw = getDrawRect();    
            float x = event->x();
            float y = event->y();
            
            screen2img(rcDraw, getData()._current_image.width(), getData()._current_image.height(), x, y);
            
            int id = findNearestPoint(getData()._current_cb_corners, x, y);
            
            if( getData()._bound_corner_ids.size() == 4){
                getData()._bound_corner_ids.clear();
            }
            getData()._bound_corner_ids.push_back(id);
            getData().establishPointOrder();
            update();
        }else if(event->buttons() & Qt::RightButton){
            float x = event->x();
            float y = event->y();
            if( getData()._bound_corner_ids.size() > 0)
                getData().clearMarks();
            else{
                //clear feature points
                QRect rcDraw = getDrawRect();                    
                screen2img(rcDraw, getData()._current_image.width(), getData()._current_image.height(), x, y);
                int id = findNearestPoint(getData()._current_cb_corners, x, y);
                cout << "remove - " << id << std::endl;
                getData().removeFeaturePoint(id);                                
            }
            update();
        }else if(event->buttons() & Qt::MidButton){
            float x = event->x();
            float y = event->y();

            //add feature points from manually clicked position
            QRect rcDraw = getDrawRect();                    
            screen2img(rcDraw, getData()._current_image.width(), getData()._current_image.height(), x, y);
            
            getData().addFeaturePoint(x,y);
            update();
        }
    }
}
