#include "DlgOption.h"
#include "ui_DlgOption.h"
#include <QMessageBox>
#include <exception>

DlgOption::DlgOption(Option& opt,QWidget *parent) :
    option(opt),
    QDialog(parent),
    ui(new Ui::DlgOption)
{
    ui->setupUi(this);
    
    ui->ed_unit->setText(QString::number(opt.unit));
    ui->ed_grid_m->setText(QString::number(opt.M));
    ui->ed_grid_n->setText(QString::number(opt.N));
}

DlgOption::~DlgOption()
{
    delete ui;
}

void DlgOption::accept(){
        bool ok;
        option.unit = ui->ed_unit->text().toDouble(&ok);
        
        if(!ok){
            QMessageBox::critical(this, "Error!", "Invalid unit value!");
            return;
        }
        
        int m = ui->ed_grid_m->text().toInt(&ok);
        
        if( !ok || m <= 0){
            QMessageBox::critical(this, "Error!", "Invalid grid value (M)!");
            return;
        }
        
        option.M = (size_t) m;
        
        int n = ui->ed_grid_n->text().toInt(&ok);
        
        if( !ok || n <= 0){
            QMessageBox::critical(this, "Error!", "Invalid grid value (M)!");
            return;
        }
        option.N = (size_t) n;
            
        QDialog::accept();

}
