/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#ifndef CHECKERBOARDCORNERCLASSIFIER_H
#define CHECKERBOARDCORNERCLASSIFIER_H
#include <opencv2/opencv.hpp>

class CheckerboardCornerClassifier
{
public:
    CheckerboardCornerClassifier();
    void genKer1(cv::Mat& ker,int hw,bool flip);
    void genKer2(cv::Mat& ker,int hw,bool flip);
    void genKer3(cv::Mat& ker,int hw,bool flip);
    void genKer4(cv::Mat& ker,int hw,bool flip);
    void genKer5(cv::Mat& ker,int hw,bool flip);
    void genKer6(cv::Mat& ker,int hw,bool flip);
    float getZNCC(const cv::Mat& gray, const cv::Point2f& corner,
                 const cv::Mat& ker);
    void apply(const cv::Mat& gray,
               const std::vector<cv::Point2f>& harris_corners,
               std::vector<cv::Point2f>& cb_corners,
               int hw = 5,
               float thres = 0.4f);
};

#endif // CHECKERBOARDCORNERCLASSIFIER_H
