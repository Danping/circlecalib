/*
 * Error.cpp
 *
 *  Created on: May 21, 2013
 *      Author: Danping Zou
 * 	 	E-mail: Dannis.zou@gmail.com
 */
#include "Error.h"
namespace sl {
Exception::Exception(void) {
}
Exception::Exception(const char* str) {
	strcpy(err_str, str);
}
Exception::~Exception(void) {

}
void warn(const char* fmtstr, ...) {
	char buf[1024];
	GET_FMT_STR(fmtstr, buf)
	fprintf(stderr, buf);
}
void throwError(const char* fmtstr, ...) {
	char buf[1024];
	GET_FMT_STR(fmtstr, buf)
	output("%s\n", buf);
	throw Exception(buf);
}

void output(const char* fmtstr, ...) {
	char buf[1024];
	GET_FMT_STR(fmtstr, buf)
	//can be replaced by other methods to output the information
	printf("%s", buf);
}
LogFile::LogFile(const char* fmtstr, ...) {
	char buf[1024];
	GET_FMT_STR(fmtstr, buf);
	fp = fopen(buf, "w+");
}
void LogFile::print(const char* fmtstr, ...) {
	char buf[1024];
	GET_FMT_STR(fmtstr, buf);
	fprintf(fp, "%s", buf);
}
}