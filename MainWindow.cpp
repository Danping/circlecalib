#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "CircleCalib.h"
#include "DlgShowRes.h"
#include "DlgOption.h"
#include "StereoCalib.h"

#include <QtWidgets>

inline void reportError(std::string info){
    QMessageBox::critical(0, QObject::tr("Error!"), QString::fromStdString(info));
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    maxRecentFolders = 5;
    ui->setupUi(this);
    setupViewUIs();
    setupActions();
    
    unit = 0.028;
    M = 7;
    N = 9;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupActions(){
    connect(ui->actionOpenDir, SIGNAL(triggered()), this, SLOT(onOpenDir()));
    connect(ui->actionPrev,SIGNAL(triggered()), this, SLOT(onPrev()));
    connect(ui->actionNext,SIGNAL(triggered()), this, SLOT(onNext()));
    connect(ui->actionExport,SIGNAL(triggered()),this, SLOT(onExport()));
    connect(ui->actionOption, SIGNAL(triggered()),this, SLOT(onOption()));
    connect(ui->actionDoCalibrationPlumbModel, SIGNAL(triggered()),this, SLOT(onDoCalibrationPlumb()));
        connect(ui->actionDoCalibrationPlumbModel4, SIGNAL(triggered()),this, SLOT(onDoCalibrationPlumb4()));
    connect(ui->actionDoCalibrationFisheyeModel, SIGNAL(triggered(bool)), this, SLOT(onDoCalibrationFisheye()));
    connect(ui->actionDoCalibrationFOVModel, SIGNAL(triggered(bool)), this, SLOT(onDoCalibrationFOV()));
    connect(ui->actionStereo_calibration, SIGNAL(triggered(bool)), this, SLOT(onStereoCalibration()));
}

void MainWindow::setupViewUIs(){
    area = new RenderArea(this);
    
    QHBoxLayout* mainLayout = new QHBoxLayout();
    ui->centralWidget->setLayout(mainLayout);
    mainLayout->setSpacing(3);
    mainLayout->setMargin(0);
    mainLayout->addWidget(area);
    
    //recent folder list    
    separatorAct = ui->menuFile->addSeparator();
    separatorAct->setVisible(false);
    
    recentFileActs.resize(maxRecentFolders);
    for (int i = 0; i < maxRecentFolders; ++i) {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        ui->menuFile->addAction(recentFileActs[i]);
        connect(recentFileActs[i], SIGNAL(triggered()),
                   this, SLOT(onOpenRecentDir()));
    }
    
    updateRecentFolderActions();
}

void MainWindow::setCurrentFolder(const QString folderPath){
    setWindowTitle(tr("%1 - %2").arg(tr("CircleDetector"))
                                       .arg(folderPath));
    QSettings settings("INS", "CircleDetector");
    QStringList files = settings.value("FolderList").toStringList();
    
    files.removeAll(folderPath);
    files.prepend(folderPath);
    
    while (files.size() > maxRecentFolders)
       files.removeLast();
    
    settings.setValue("FolderList", files);
    updateRecentFolderActions();
}

void MainWindow::updateRecentFolderActions(){
    QSettings settings("INS", "CircleDetector");
    QStringList files = settings.value("FolderList").toStringList();

    int numRecentFiles = qMin(files.size(), maxRecentFolders);

    for (int i = 0; i < numRecentFiles; ++i) {
        QString text = tr("[&%1] - %2").arg(i + 1).arg( QFileInfo((files[i])).fileName());
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
    for (int j = numRecentFiles; j < maxRecentFolders; ++j)
        recentFileActs[j]->setVisible(false);

    separatorAct->setVisible(numRecentFiles > 0);   
}

void MainWindow::refresh(){
    area->update();
}

void MainWindow::openDir(QString dir){
    getData().reset();
    refresh();
    
    if( getData().listAllImages(string(dir.toLocal8Bit()))){
        if( !getData().jumpTo(0))
            reportError("cannot load the image!");
        else{
            refresh();
            setCurrentFolder(dir);
        }
    }   
}

void MainWindow::onOpenDir(){
    QString dir = QFileDialog::getExistingDirectory(this,tr("Select the folder containing the images for calibration ..."),
                                                    tr(""),
            QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
   
    openDir(dir);
}

void MainWindow::onOpenRecentDir(){
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
       openDir(action->data().toString());   
}
void MainWindow::onPrev(){
    if( getData().prev())
        refresh();
}
void MainWindow::onNext(){
    if( getData().next())
        refresh();
}

void MainWindow::onExport(){   
    if( getData().exportFeatPts()){
        QMessageBox::information(this, QObject::tr("Export feature points..."), tr("OK!"));   
    }    
}
void MainWindow::onOption(){
    Option opt;
    opt.unit = unit;
    opt.M = M;
    opt.N = N;
    DlgOption dlg(opt, this);

    dlg.show();
    if(dlg.exec() == QDialog::Accepted){
        unit = opt.unit;
        M = opt.M;
        N = opt.N;               
    }
}

void MainWindow::onDoCalibrationPlumb(){
    if( getData().doCalibration(unit, CircleCalib::TypePlumb)) {        
        DlgShowRes dlg(this);
        dlg.show();
        dlg.exec();
    }else
        QMessageBox::information(this, tr("Do calibration (Plumb-Bob Model)..."), tr("Error!"));
}

void MainWindow::onDoCalibrationPlumb4(){
    if( getData().doCalibration(unit, CircleCalib::TypePlumb4)) {        
        DlgShowRes dlg(this);
        dlg.show();
        dlg.exec();
    }else
        QMessageBox::information(this, tr("Do calibration (Plumb-Bob4 Model)..."), tr("Error!"));
}


void MainWindow::onDoCalibrationFisheye()
{
    if( getData().doCalibration(unit, CircleCalib::TypeFisheye)){
        DlgShowRes dlg(this);
        dlg.show();
        dlg.exec();
    }else
        QMessageBox::information(this, tr("Do calibration (fisheye model)..."), tr("Error!"));
}

void MainWindow::onDoCalibrationFOV()
{
    if( getData().doCalibration(unit, CircleCalib::TypeFOV)){
        DlgShowRes dlg(this);
        dlg.show();
        dlg.exec();
    }else
        QMessageBox::information(this, tr("Do calibration (FOV model)..."), tr("Error!"));
}

void MainWindow::onStereoCalibration()
{
    QString dir = QFileDialog::getExistingDirectory(this,
                                                    tr("Select the folder containing the stereo images..."),
                                                    tr(""),
            QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    
    Option opt;
    opt.unit = unit;
    opt.M = M;
    opt.N = N;
    DlgOption dlg(opt, this);
    dlg.show();
    if(dlg.exec() == QDialog::Accepted){
        StereoCalib stereo_calib(dir.toStdString(), opt.M, opt.N, unit);        
        stereo_calib.doCalibration();
        std::ostringstream ss;
        ss << "The relative pose is saved at '" << dir.toStdString() << "/stereo_qp_lr.txt'. ";
        QMessageBox::information(this, tr("Finished!"), QString::fromStdString(ss.str()));
    
    }
}
