#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include "RenderArea.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    RenderArea* area;
    int maxRecentFolders;
    std::vector<QAction*> recentFileActs;
    QAction* separatorAct;
    
public:
    double unit;
    size_t M,N;
    void setupActions();
    void setupViewUIs();
    void setCurrentFolder(const QString folderPath);
    void updateRecentFolderActions();
    void refresh();
    
    void openDir(QString dir);
protected slots:
    void onOpenDir();
    void onOpenRecentDir();
    void onPrev();
    void onNext();
    void onExport();
    void onOption();
    void onDoCalibrationPlumb();
    void onDoCalibrationPlumb4();
    void onDoCalibrationFisheye();
    void onDoCalibrationFOV();
    void onStereoCalibration();
};

#endif // MAINWINDOW_H
