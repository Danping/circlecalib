/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* 
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#ifndef EIGENTYPES_H
#define EIGENTYPES_H

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Geometry>
typedef float float_type;
typedef Eigen::Triplet<float_type>  ETriplet;
typedef Eigen::Matrix<float_type,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EMat;
typedef Eigen::Matrix<std::complex<float_type>, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ECMat;
typedef Eigen::Map<EMat> EMatMap;
typedef Eigen::Map<ECMat> ECMatMap;
typedef Eigen::Map<const EMat> ConstEMatMap;
typedef Eigen::Matrix<float_type,Eigen::Dynamic, 1> EVec;
typedef Eigen::Matrix<std::complex<float_type>,Eigen::Dynamic, 1> ECVec;
typedef Eigen::SparseMatrix<float_type> ESpMat;  
typedef Eigen::Matrix<float_type,3,1> EVec3;
typedef Eigen::Matrix<float_type,3,3,Eigen::RowMajor> EMat33;
typedef Eigen::Quaternion<float_type> EQuat;
typedef Eigen::Map<EQuat> EQuatMap;
typedef Eigen::Map<const EQuat> ConstEQuatMap;
#endif // EIGENTYPES_H
