#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QMouseEvent>
class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = 0);  
protected:
    QRect getDrawRect();
    virtual void paintEvent(QPaintEvent *);        
    virtual void mousePressEvent(QMouseEvent *);
};


#endif // RENDERAREA_H
