/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#include "CalibOptimization.h"
#include "EigenTypes.h"
#include "ceres/problem.h"
#include "ceres/solver.h"
#include "ceres/autodiff_cost_function.h"
#include "ceres/local_parameterization.h"
#include <cmath>

CalibOptimization::CalibOptimization()
{
    
}

void CalibOptimization::apply(const CalibOptimization::T *kvec, const CalibOptimization::T *dvec, const std::vector<std::vector<cv::Point3f> > vec_objpts, const std::vector<std::vector<cv::Point2f> > vec_imgpts, const std::vector<double> vec_poses, int type)
{
      _kvec.resize(4);
      std::copy(kvec,kvec+4,_kvec.begin());
      _type = type;
      switch(type){
      case TypePlumb:
          _dvec.resize(5,1);
          std::copy(dvec,dvec+5,_dvec.begin());
          break;
      case TypePlumb4:
          _dvec.resize(4,1);
          std::copy(dvec,dvec+4,_dvec.begin());
          break;
      case TypeFisheye:
          _dvec.resize(4,1);
          std::copy(dvec,dvec+4,_dvec.begin());
          break;
      case TypeFOV:
          _dvec.resize(1,1);
          _dvec[0] = dvec[0];
          break;
      }
      
      _qpvec = vec_poses;
      ceres::Problem problem;
      
      //1.add all observations
      for( size_t k = 0; k < vec_objpts.size(); k++){
          for( size_t i = 0; i < vec_objpts[k].size(); i++){
              double M[3];
              M[0] = vec_objpts[k][i].x;
              M[1] = vec_objpts[k][i].y;
              M[2] = vec_objpts[k][i].z;
              //add a 2d-3d correspondence
              ceres::CostFunction* cost_function
                      = new ceres::AutoDiffCostFunction<
                      ReprojectionError,2,4,1,7>(
                          new ReprojectionError(_type,
                                                   vec_imgpts[k][i].x,
                                                   vec_imgpts[k][i].y,
                                                   M));
              problem.AddResidualBlock(cost_function, 0, &_kvec[0],&_dvec[0],
                      &_qpvec[k*7]);
          }
      }
      
      //2.set local parameterization for camera poses
      ceres::LocalParameterization* camera_parameterization =
              new ceres::ProductParameterization(
                  new ceres::QuaternionParameterization(),
                  new ceres::IdentityParameterization(3));
      
      for( size_t k = 0; k < vec_objpts.size(); k++){
          problem.SetParameterization(&_qpvec[k*7],camera_parameterization);
      }
      
      //3. solve the problem
      ceres::Solver::Options options;
      options.minimizer_progress_to_stdout = true;
      ceres::Solver::Summary summary;
      ceres::Solve(options, &problem, &summary);
      std::cout << summary.FullReport() << "\n";
      
}

void CalibOptimization::apply(const cv::Mat &K0,
                              const std::vector<std::vector<cv::Point3f> > vec_objpts,
                              const std::vector<std::vector<cv::Point2f> > vec_imgpts,
                              const std::vector<cv::Mat> &rvecs, 
                              const std::vector<cv::Mat> &tvecs,
                              int type)
{
    //copy or set the intial parameters
    _kvec.resize(4);
    _kvec[0] = K0.at<double>(0,0);
    _kvec[1] = K0.at<double>(1,1);
    _kvec[2] = K0.at<double>(0,2);
    _kvec[3] = K0.at<double>(1,2);           
    _type = type;
    
    switch(type){
    case TypePlumb:
        _dvec.resize(5,1);
        std::fill_n(_dvec.begin(),5,0);
        break;
    case TypePlumb4:
        _dvec.resize(4,1);
        std::fill_n(_dvec.begin(),4,0);
        break;
    case TypeFisheye:
        _dvec.resize(4,1);
        std::fill_n(_dvec.begin(),4,0);
        _dvec[0] = 0.0;
        break;
    case TypeFOV:
        _dvec.resize(1,1);
        _dvec[0] = 2.0;
        break;
    }
    
    _qpvec.resize(rvecs.size()*7);
    T* x_ptr = &_qpvec[0];
    for( size_t k = 0; k < rvecs.size(); k++){
        cv::Mat rvec = rvecs[k];
        ceres::AngleAxisToQuaternion((T*) rvec.data, x_ptr);
        cv::Mat tvec = tvecs[k];
        x_ptr[4] = tvec.at<double>(0,0);
        x_ptr[5] = tvec.at<double>(0,1);
        x_ptr[6] = tvec.at<double>(0,2);
        x_ptr += 7;                
    }
    
    ceres::Problem problem;
    
    //1.add all observations
    for( size_t k = 0; k < vec_objpts.size(); k++){
        for( size_t i = 0; i < vec_objpts[k].size(); i++){
            double M[3];
            M[0] = vec_objpts[k][i].x;
            M[1] = vec_objpts[k][i].y;
            M[2] = vec_objpts[k][i].z;
            //add a 2d-3d correspondence
            ceres::CostFunction* cost_function = 
                    ReprojectionError::create(_type,
                                              vec_imgpts[k][i].x,
                                              vec_imgpts[k][i].y,
                                              M);
            problem.AddResidualBlock(cost_function, 0, &_kvec[0],&_dvec[0],
                    &_qpvec[k*7]);
        }
    }
    
    //2.set local parameterization for camera poses
    ceres::LocalParameterization* camera_parameterization =
            new ceres::ProductParameterization(
                new ceres::QuaternionParameterization(),
                new ceres::IdentityParameterization(3));
    
    for( size_t k = 0; k < vec_objpts.size(); k++){
        problem.SetParameterization(&_qpvec[k*7],camera_parameterization);
    }
    
    //3. solve the problem
    ceres::Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    std::cout << summary.FullReport() << "\n";
}

void CalibOptimization::output(cv::Mat &K, cv::Mat &dc)
{
    K = cv::Mat(3,3,CV_64FC1);
    K.setTo(0);
    K.at<double>(0,0) = _kvec[0];
    K.at<double>(1,1) = _kvec[1];
    K.at<double>(0,2) = _kvec[2];
    K.at<double>(1,2) = _kvec[3];
    K.at<double>(2,2) = 1.0;

    dc = cv::Mat(1,5,CV_64FC1);
    dc.at<double>(0,0) = _dvec[0];
    if( _type == TypeFOV){
        dc.at<double>(0,1) = INVALID_DISTORTION_VALUE;
        dc.at<double>(0,2) = INVALID_DISTORTION_VALUE;
        dc.at<double>(0,3) = INVALID_DISTORTION_VALUE;
        dc.at<double>(0,4) = INVALID_DISTORTION_VALUE;
    }
    else if( _type ==TypeFisheye){
        dc.at<double>(0,1) = _dvec[1];
        dc.at<double>(0,2) = _dvec[2];
        dc.at<double>(0,3) = _dvec[3];
        dc.at<double>(0,4) = INVALID_DISTORTION_VALUE;
    }else if(_type == TypePlumb){
        dc.at<double>(0,1) = _dvec[1];
        dc.at<double>(0,2) = _dvec[2];
        dc.at<double>(0,3) = _dvec[3];
        dc.at<double>(0,4) = _dvec[4];
    }else if(_type == TypePlumb4){
        dc.at<double>(0,1) = _dvec[1];
        dc.at<double>(0,2) = _dvec[2];
        dc.at<double>(0,3) = _dvec[3];   
        dc.at<double>(0,4) = 0;        
    }
}
