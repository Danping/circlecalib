#include "Utilities.h"
void mat2QImage(const cv::Mat& mat, QImage& img){
    QImage tmp = QImage((uchar*) mat.data, mat.cols, mat.rows,
                        mat.step, QImage::Format_RGB888);
    img = tmp.copy();
}

void qImage2Mat(const QImage& orgImg, cv::Mat& mat){
    QImage img = orgImg.convertToFormat(QImage::Format_RGB888);
    mat = cv::Mat(img.height(), img.width(), CV_8UC3,
                  (uchar*) img.bits(), img.bytesPerLine()).clone();
}
