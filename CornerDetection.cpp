#include "CornerDetection.h"

using namespace std;
using namespace cv;

const double PI = 3.141592653589793238462;

float getInnerAngle(size_t ind, const vector<Point2f>& pts) {
	assert(size_t(ind) >= 0 && size_t(ind) < pts.size());

	float p1[2], p2[2];
	if (ind == 0) {
		p1[0] = pts.back().x - pts[0].x;
		p1[1] = pts.back().y - pts[0].y;

		p2[0] = pts[1].x - pts[0].x;
		p2[1] = pts[1].y - pts[0].y;

	} else if (ind == pts.size() - 1) {
		p1[0] = pts[pts.size() - 2].x - pts.back().x;
		p1[1] = pts[pts.size() - 2].y - pts.back().y;

		p2[0] = pts.front().x - pts.back().x;
		p2[1] = pts.front().y - pts.back().y;
	} else {
		p1[0] = pts[ind - 1].x - pts[ind].x;
		p1[1] = pts[ind - 1].y - pts[ind].y;

		p2[0] = pts[ind + 1].x - pts[ind].x;
		p2[1] = pts[ind + 1].y - pts[ind].y;
	}

	float cs = (p1[0] * p2[0] + p1[1] * p2[1])
			/ (sqrt(p1[0] * p1[0] + p1[1] * p1[1])
					* sqrt(p2[0] * p2[0] + p2[1] * p2[1]));

	return acos(cs) / PI * 180;
}

float getCenterAngle(size_t ind, const vector<Point2f>& pts) {
	float p1[2], p2[2];
	if (ind == 0) {
		p1[0] = pts.back().x - pts[0].x;
		p1[1] = pts.back().y - pts[0].y;

		p2[0] = pts[1].x - pts[0].x;
		p2[1] = pts[1].y - pts[0].y;

	} else if (ind == pts.size() - 1) {
		p1[0] = pts[pts.size() - 2].x - pts.back().x;
		p1[1] = pts[pts.size() - 2].y - pts.back().y;

		p2[0] = pts.front().x - pts.back().x;
		p2[1] = pts.front().y - pts.back().y;
	} else {
		p1[0] = pts[ind - 1].x - pts[ind].x;
		p1[1] = pts[ind - 1].y - pts[ind].y;

		p2[0] = pts[ind + 1].x - pts[ind].x;
		p2[1] = pts[ind + 1].y - pts[ind].y;
	}

	float sp1 = sqrt(p1[0]*p1[0] + p1[1]*p1[1]);
	float sp2 = sqrt(p2[0]*p2[0] + p2[1]*p2[1]);
	float cp[2];
	cp[0] = (p1[0]/sp1 + p2[0]/sp2) / 2;
	cp[1] = (p1[1]/sp1 + p2[1]/sp2) / 2;

	float alpha = atan2(-cp[1], cp[0]) / PI * 180 + 45;
	alpha = alpha > 360 ? alpha- 360:alpha;
	return alpha;
}

bool findFourCorners(const vector<Point2f>& pts, const vector<int>& hull,
		vector<int>& corner, size_t MGRIDS, size_t NGRIDS) {
	if ((int)pts.size() != MGRIDS * NGRIDS)
		return false;

	int min_ind[4] = { -1, -1, -1, -1 };
	float min_val[4] = { 360, 360, 360, 360 };

	vector<Point2f> hullpts;

	for (size_t i = 0; i < hull.size(); i++) {
		int ind = hull[i];
		hullpts.push_back(pts[ind]);
	}

	for (size_t i = 0; i < hullpts.size(); i++) {

		float dd = getInnerAngle(i, hullpts);

		if (dd < min_val[0]) {
			min_val[3] = min_val[2];
			min_val[2] = min_val[1];
			min_val[1] = min_val[0];
			min_val[0] = dd;

			min_ind[3] = min_ind[2];
			min_ind[2] = min_ind[1];
			min_ind[1] = min_ind[0];
			min_ind[0] = i;
		} else if (dd < min_val[1]) {
			min_val[3] = min_val[2];
			min_val[2] = min_val[1];
			min_val[1] = dd;

			min_ind[3] = min_ind[2];
			min_ind[2] = min_ind[1];
			min_ind[1] = i;
		} else if (dd < min_val[2]) {
			min_val[3] = min_val[2];
			min_val[2] = dd;

			min_ind[3] = min_ind[2];
			min_ind[2] = i;
		} else if (dd < min_val[3]) {
			min_val[3] = dd;
			min_ind[3] = i;
		}
	}

	if (min_ind[3] < 0)
		return false;

	//check sum of inner angle
	float a1 = min_val[0];
	float a2 = min_val[1];
	float a3 = min_val[2];
	float a4 = min_val[3];

	float sa = a1 + a2 + a3 + a4;

	float dt = fabs(sa - 360);
	if (dt > 35)
		return false;

	corner.push_back(hull[min_ind[0]]);
	corner.push_back(hull[min_ind[1]]);
	corner.push_back(hull[min_ind[2]]);
	corner.push_back(hull[min_ind[3]]);

	vector<pair<float, int> > ordered_ind;
	for (int i = 0; i < 4; i++) {
		int id_in_hull = min_ind[i];
		float angle = getCenterAngle(id_in_hull, hullpts);
		ordered_ind.push_back(make_pair(angle, id_in_hull));
	}

	sort(ordered_ind.begin(), ordered_ind.end());

	corner.clear();
    corner.push_back(hull[ordered_ind[1].second]);
    corner.push_back(hull[ordered_ind[0].second]);
    corner.push_back(hull[ordered_ind[3].second]);
    corner.push_back(hull[ordered_ind[2].second]);

	return true;
}



