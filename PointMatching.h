/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* 
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#ifndef POINTMATCHING_H
#define POINTMATCHING_H
#include <opencv2/opencv.hpp>

class PointMatching
{
public:
    PointMatching();
    
    std::vector<std::vector<double> > dist_mat;
    
    void buildDistanceMatrix(const std::vector<cv::Point2f>& pts, double max_range);

    bool checkProblematicConnection( const std::vector<cv::Point2f>& pts,
                                     const std::vector<int>& ids,
                                    std::vector<std::pair<int,int> >& vec_conn);
    
    void getCloseIds(const std::vector<cv::Point2f> &pts, 
                     int id_cur,
                     std::vector<int>& close_ids);
    
    bool attemptMatch(const std::vector<cv::Point2f> &pts, 
                      int id_start,
                      int id_end,
                      std::vector<int> &ids,
                      size_t N);
    
    bool matchOneRow(const std::vector<cv::Point2f> &pts, 
                      int id_start,
                      int id_end,
                      std::vector<int> &ids,
                      size_t N);
};

#endif // POINTMATCHING_H