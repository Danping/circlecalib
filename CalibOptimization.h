/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#ifndef CALIBOPTIMIZATION_H
#define CALIBOPTIMIZATION_H
#include <vector>
#include <opencv2/opencv.hpp>
#include "ceres/rotation.h"
#include "ceres/autodiff_cost_function.h"
#define INVALID_DISTORTION_VALUE -10000

template<typename T>
void _distorNormPoint_plumb(const T kc[5], T un, T vn, T& und, T& vnd){
    T xn2 = un*un;
    T yn2 = vn*vn;
    T xn_yn = un*vn;
    T r2 = xn2 + yn2;
    T dist = (r2*(kc[0] + r2*(kc[1] + r2*kc[4]))+T(1));
    T tx = kc[2]*xn_yn*T(2) + kc[3]*(r2 + xn2*T(2));
    T ty = kc[2]*(r2+yn2*T(2)) + kc[3]*xn_yn*T(2);
    
    und = un*dist+tx;
    vnd = vn*dist+ty;
}

template<typename T>
void _distorNormPoint_fov(const T* kc, T un, T vn, T& und, T& vnd){
   T xn2 = un*un;
   T yn2 = vn*vn;
   T r2 = xn2 + yn2;
   T r = sqrt(r2);
   
   T rd = atan(r*tan(kc[0]*0.5)*2.0)/kc[0];
   T rtrans = r < T(0.001) ? T(1.0):(rd/r);
   
   und = un*rtrans;
   vnd = vn*rtrans;
}

template<typename T>
void _distorNormPoint_fisheye(const T* kc, T un, T vn, T& und, T& vnd){    
    T xn2 = un*un;
    T yn2 = vn*vn;
    T r2 = xn2 + yn2;
    T r = sqrt(r2);
    T theta = atan(r);
    T theta2 = theta*theta;
    T theta4 = theta2*theta2;
    T theta6 = theta2*theta4;
    T theta8 = theta4*theta4;
    T theta_d = theta*(T(1)+kc[0]*theta2+kc[1]*theta4+kc[2]*theta6+kc[3]*theta8);
    
    und = (theta_d/r)*un;
    vnd = (theta_d/r)*vn;
}


template<typename T>
void projectFOV(const T* kvec,
                const T* dvec,
                const T* camera,
                const T* M,
                T& rx,
                T& ry){
    T p[3];
    ceres::QuaternionRotatePoint(camera, M, p);
//    std::cout << "M:" << M[0] <<  " " << M[1] << " " << M[2] << std::endl;
    p[0] += camera[4];
    p[1] += camera[5];
    p[2] += camera[6];
    
    T u0 = p[0]/p[2];
    T v0 = p[1]/p[2];
    T ud, vd;
    _distorNormPoint_fov(dvec,u0,v0,ud,vd);
    rx = kvec[0]*ud+kvec[2];
    ry = kvec[1]*vd+kvec[3];
}

template<typename T>
void projectFisheye(const T* kvec,
                    const T* dvec,
                    const T* camera,
                    const T* M,
                    T& rx,
                    T& ry){
    T p[3];
    ceres::QuaternionRotatePoint(camera, M, p);
//    std::cout << "M:" << M[0] <<  " " << M[1] << " " << M[2] << std::endl;
    p[0] += camera[4];
    p[1] += camera[5];
    p[2] += camera[6];
    
    T u0 = p[0]/p[2];
    T v0 = p[1]/p[2];
    T ud, vd;
    _distorNormPoint_fisheye(dvec,u0,v0,ud,vd);
    rx = kvec[0]*ud+kvec[2];
    ry = kvec[1]*vd+kvec[3];    
}

template<typename T>
void projectPlumb(const T* kvec,
                  const T* dvec,
                  const T* camera,
                  const T* M,
                  T& rx,
                  T& ry){
    T p[3];
    ceres::QuaternionRotatePoint(camera, M, p);
//    std::cout << "M:" << M[0] <<  " " << M[1] << " " << M[2] << std::endl;
    p[0] += camera[4];
    p[1] += camera[5];
    p[2] += camera[6];
    
    T u0 = p[0]/p[2];
    T v0 = p[1]/p[2];
    T ud, vd;
    _distorNormPoint_plumb(dvec,u0,v0,ud,vd);
    rx = kvec[0]*ud+kvec[2];
    ry = kvec[1]*vd+kvec[3];    
}

template<typename T>
void projectPlumb4(const T* kvec,
                  const T* dvec,
                  const T* camera,
                  const T* M,
                  T& rx,
                  T& ry){
    T p[3];
    ceres::QuaternionRotatePoint(camera, M, p);
//    std::cout << "M:" << M[0] <<  " " << M[1] << " " << M[2] << std::endl;
    p[0] += camera[4];
    p[1] += camera[5];
    p[2] += camera[6];
    
    T u0 = p[0]/p[2];
    T v0 = p[1]/p[2];
    T ud, vd;
    
    T d5[5];
    d5[0] = dvec[0];
    d5[1] = dvec[1];
    d5[2] = dvec[2];
    d5[3] = dvec[3];
    d5[4] = T(0);
    _distorNormPoint_plumb(d5,u0,v0,ud,vd);
    rx = kvec[0]*ud+kvec[2];
    ry = kvec[1]*vd+kvec[3];    
}


struct ReprojectionError{
    enum{
        TypePlumb = 0,
        TypePlumb4 = 1,
        TypeFisheye = 2,
        TypeFOV = 3
    };
    
    typedef double Real;
    ReprojectionError(int type, Real ox, Real oy, const Real pt[3]){
        _type = type;
        _ox = ox;
        _oy = oy;
        _pt[0] = pt[0];
        _pt[1] = pt[1];
        _pt[2] = pt[2];
    }    
    template<typename T>
    bool operator()(const T* kvec,
                    const T* dvec,
                    const T* camera,
                    T* residuals) const{
        T rx,ry;
        /* be particually attention not to cast pointer types in this function
         * as ceres converts all values into ceres::Jet objects!*/
        T M[3];
        M[0] = (T)(_pt[0]);
        M[1] = (T)(_pt[1]);
        M[2] = (T)(_pt[2]);
        if(_type == TypePlumb)
            projectPlumb(kvec,dvec,camera, M,rx, ry);        
        else if(_type == TypePlumb4)
            projectPlumb4(kvec,dvec,camera, M,rx, ry);        
        else if(_type == TypeFisheye)
            projectFisheye(kvec,dvec,camera, M,rx, ry);        
        else if(_type == TypeFOV)
            projectFOV(kvec,dvec,camera, M,rx, ry);        
        
        residuals[0] = _ox - rx;
        residuals[1] = _oy - ry;
        return true;
    }
    int _type;
    Real _ox,_oy;
    Real _pt[3];
    static ceres::CostFunction* create(int type,
                                       Real x,
                                       Real y,
                                       const Real M[3]){
        ReprojectionError* rep = new ReprojectionError(type,x,y,M);
        ceres::CostFunction* cost_function  = 0;
        if(type == TypeFOV)
            cost_function = new ceres::AutoDiffCostFunction<ReprojectionError,2,4,1,7>(rep);
        else if(type == TypeFisheye)
            cost_function = new ceres::AutoDiffCostFunction<ReprojectionError,2,4,4,7>(rep);
        else if(type == TypePlumb)
            cost_function = new ceres::AutoDiffCostFunction<ReprojectionError,2,4,5,7>(rep);
        else if(type == TypePlumb4)
            cost_function = new ceres::AutoDiffCostFunction<ReprojectionError,2,4,4,7>(rep);
        return cost_function;
    }
};

class CalibOptimization
{
public:
    typedef double T;
    
    /* camera intrinsic parameters (4x1 fx,fy,cx,cy)*/
    std::vector<T> _kvec;
    /* camera distortion parameters (plumb-bob - 5x1 or (4x1), fisheye - 4x1, FOV - 1x1)*/
    std::vector<T> _dvec;
    /* camera pose for each image*/
    std::vector<T> _qpvec;
    
    enum{
        TypePlumb = 0,
        TypePlumb4 = 1,
        TypeFisheye = 2,
        TypeFOV = 3
    };
    
    int _type;
    
    CalibOptimization();
    
    void apply(const T* kvec,
               const T* dvec,
               const std::vector<std::vector<cv::Point3f> > vec_objpts,
               const std::vector<std::vector<cv::Point2f> > vec_imgpts,
               const std::vector<double> vec_poses,
               int type = TypeFOV);
    
    void apply(const cv::Mat& K0,
               const std::vector<std::vector<cv::Point3f> > vec_objpts,
               const std::vector<std::vector<cv::Point2f> > vec_imgpts,
               const std::vector<cv::Mat>& rvecs,
               const std::vector<cv::Mat>& tvecs,
               int type = TypeFOV);
    
    void output(cv::Mat& K,
                cv::Mat& dc);
};

#endif // CALIBOPTIMIZATION_H
