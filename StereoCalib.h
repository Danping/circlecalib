/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* 
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#ifndef STEREOCALIB_H
#define STEREOCALIB_H

#include <QFile>
#include <QDir>

#include <iostream>
#include <fstream>

#include "opencv2/opencv.hpp"
#include "Error.h"



class StereoCalib
{
public:
    int M,N;
    double unit;
    std::string dir;
    std::string imgext;
    StereoCalib(std::string dir_, int M_, int N_, double unit_){
        dir = dir_;
        M = M_;
        N = N_;
        unit = unit_;
        imgext = "png";
    }
    void doCalibration();
};

#endif // STEREOCALIB_H