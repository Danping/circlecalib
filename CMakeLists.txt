project(CircleCalib)
cmake_minimum_required(VERSION 2.8)
#add the path to the installed OpenCV-3.1.0 
#list(APPEND CMAKE_PREFIX_PATH $ENV{OPENCV_DIR}/share/OpenCV)

find_package(Qt5Widgets REQUIRED)
find_package(OpenCV 3 REQUIRED)
#find_package(OpenCV 2.4.9 REQUIRED PATHS $ENV{OpenCV2_DIR}/share/OpenCV)
find_package(Ceres REQUIRED)

include_directories(${CMAKE_BINARY_DIR})
include_directories(${CERES_INCLUDE_DIRS})

QT5_WRAP_UI(UIS_HDRS MainWindow.ui
		     DlgOption.ui
	             DlgShowRes.ui)

QT5_WRAP_CPP(MOC_CPPS MainWindow.h
		      DlgOption.h
		      DlgShowRes.h
                      RenderArea.h)

QT5_ADD_RESOURCES(RESOURCES CircleCalib.qrc)

add_executable(${PROJECT_NAME} 
               main.cpp
               CalibOptimization.cpp
               CircleCalib.cpp
               CornerDetection.cpp
               Error.cpp
               LAP.cpp
               PointMatching.cpp
               RenderArea.cpp
               StereoCalib.cpp
               Utilities.cpp
               MainWindow.cpp
               DlgOption.cpp
               DlgShowRes.cpp
               CheckerboardCornerClassifier.cpp
               ${UIS_HDRS}
               ${MOC_CPPS}
               ${RESOURCES})

qt5_use_modules(${PROJECT_NAME} Widgets)

target_link_libraries(${PROJECT_NAME}
            Qt5::Widgets
	    ${OpenCV_LIBRARIES}
            ${CERES_LIBRARIES})
        
add_executable(unit_test
               mainTest.cpp
               CheckerboardCornerClassifier.cpp
               CalibOptimization.cpp)
           
target_link_libraries(unit_test
           ${OpenCV_LIBRARIES}
           ${CERES_LIBRARIES}
           )
