/*
 * Error.h
 *
 *  Created on: May 21, 2013
 *      Author: Danping Zou
 * 	 	E-mail: Dannis.zou@gmail.com
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

#define GET_FMT_STR(fmstr,buf) \
	va_list args;\
	va_start(args,fmtstr);\
	vsprintf(buf,fmstr,args);\
	va_end(args);

namespace sl {

class Exception {
protected:
	char err_str[1024];
public:
	Exception(void);
	Exception(const char* str);
	~Exception(void);
	const char* what() const {
		return err_str;
	}
};
//report a warning without interrupting the execution of the program
void warn(const char* fmtstr, ...);
//report an error exception
void throwError(const char* fmtstr, ...);
//just log out information
void output(const char* fmtstr, ...);

class LogFile {
public:
	FILE* fp;
public:
	LogFile(const char* fmtstr, ...);
	~LogFile() {
		if (fp)
			fclose(fp);
	}
	void print(const char* fmtstr, ...);
	void close() {
		fclose(fp);
		fp = 0;
	}
};

}
#endif /* ERROR_H_ */
