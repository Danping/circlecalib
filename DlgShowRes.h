#ifndef DLGSHOWRES_H
#define DLGSHOWRES_H

#include <QDialog>

namespace Ui {
class DlgShowRes;
}

class DlgShowRes : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgShowRes(QWidget *parent = 0);
    ~DlgShowRes();
    
private:
    Ui::DlgShowRes *ui;
};

#endif // DLGSHOWRES_H
