/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* 
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#include "PointMatching.h"
#include <numeric>
#include <limits>
 

static void getDirection(const cv::Point2f& m0,
                         const cv::Point2f& m1,
                         double dir[2]){
    dir[0] = m1.x - m0.x;
    dir[1] = m1.y - m0.y;    
}

static double getDistance(const cv::Point2f& m0,
                          const cv::Point2f& m1){
    double dir[2];
    dir[0] = m1.x - m0.x;
    dir[1] = m1.y - m0.y;
    
    double s = sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
    return s;        
}
PointMatching::PointMatching()
{
}

void PointMatching::buildDistanceMatrix(const std::vector<cv::Point2f> &pts, 
                                        double max_range)
{
    dist_mat.resize(pts.size());
    for( size_t i = 0; i < pts.size(); i++){
        dist_mat[i].resize(pts.size());
       for( size_t j = 0; j < pts.size(); j++){
           double dist = getDistance(pts[i],pts[j]);
           if( i == j || dist > max_range)
               dist_mat[i][j] = -1;
           else
               dist_mat[i][j] = dist;
       }
    }
}

bool PointMatching::checkProblematicConnection(const std::vector<cv::Point2f>& pts,
                                               const std::vector<int> &ids,
                                               std::vector<std::pair<int, int> > &vec_conn)
{
    
    vec_conn.clear();
    for( size_t k = 1; k < ids.size(); k++){
        int id = ids[k-1];
        bool has_problem = false;
        double dist_edge = dist_mat[id][ids[k]];
        for( size_t s = 0; s < dist_mat[id].size(); s++){
            double d1 = dist_mat[id][s];
            double d2 = dist_mat[s][ids[k]];
            if( d1 > 0 && d2 > 0 && d1+d2 < 1.05*dist_edge){
                has_problem = true;
            }   
        }
        
        if(has_problem){
            vec_conn.push_back(std::make_pair(id,ids[k]));
            break;
        }
    }
    return !vec_conn.empty();
}

void PointMatching::getCloseIds(const std::vector<cv::Point2f> &pts, 
                                int id_cur,
                                std::vector<int>& close_ids){
    close_ids.clear();
    for( size_t i = 0; i < dist_mat[id_cur].size(); i++){
        if( dist_mat[id_cur][i] > 0)
            close_ids.push_back((int) i);
    }
}

static double getSmoothness(const std::vector<cv::Point2f> &pts,
                          const double dir[2],
                          const int cur_id,
                          const int next_id){
    double pred_x = pts[cur_id].x + dir[0];
    double pred_y = pts[cur_id].y + dir[1];
    
//    double pred_x = pts[cur_id].x;
//    double pred_y = pts[cur_id].y;
    
    
    double dx = pts[next_id].x - pred_x;
    double dy = pts[next_id].y - pred_y;
    
    return (dx*dx+dy*dy);
}
bool PointMatching::attemptMatch(const std::vector<cv::Point2f> &pts,
                                 int id_start,
                                 int id_end, 
                                 std::vector<int> &ids,
                                 size_t N)
{
    double dir_ref[2],dir[2];
    
    getDirection(pts[id_start],pts[id_end], dir_ref);
    dir_ref[0]/=(N-1);
    dir_ref[1]/=(N-1);
    
    
    std::vector<double> smoothness;
    std::vector<int> prev_id;
    std::vector<int> length;
    
    const double big_value = std::numeric_limits<double>::max();
    smoothness.assign(pts.size(),big_value);
    
    prev_id.assign(pts.size(),-1);
    length.assign(pts.size(), 1);
    
    std::deque<int> Q;
    std::vector<int> close_ids;
    
    Q.push_back(id_start);    
    
    smoothness[id_start] = 0;
    length[id_start] = 1;
        
    int min_id = id_start;    
    
    /* use breadth-first searching to find a smoothness path from id_start to id_end*/
    while(!Q.empty()){
        min_id = Q.front();
        Q.pop_front();
        if( min_id == id_end)
            break;
                
        getCloseIds(pts, min_id, close_ids);
        
        if( prev_id[min_id] < 0){
            dir[0] = dir_ref[0];
            dir[1] = dir_ref[1];
        }else{
            getDirection(pts[prev_id[min_id]], pts[min_id],dir);
        }
        
        for( size_t i = 0; i < close_ids.size(); i++){
            int cur_id = close_ids[i];
            if(smoothness[cur_id] > big_value/2)
                Q.push_back(cur_id); 
//            int l = length[min_id];
//            double new_s = (smoothness[min_id]*l 
//                    + getSmoothness(pts, dir, min_id, cur_id))/(l+1);
            double new_s = smoothness[min_id]+getSmoothness(pts, dir, min_id, cur_id);
            if( new_s < smoothness[cur_id]){                
                smoothness[cur_id] = new_s;
                prev_id[cur_id] = min_id;
                length[cur_id] = length[min_id]+1;    
            }           
        }   
    }
    
    ids.clear();
    /* traverse back to the the path*/
    std::deque<int> path;
    
    int id = id_end;
    while(id >= 0){
        path.push_back(id);
        id = prev_id[id];
    }
    
    for( size_t i = 0; i < path.size(); i++)
        ids.push_back(path[i]);
    
    return true;
}

bool PointMatching::matchOneRow(const std::vector<cv::Point2f> &pts, 
                                int id_start,
                                int id_end,
                                std::vector<int> &ids, size_t N)
{
    assert(N >= 3);
    
    double max_range = getDistance(pts[id_start],pts[id_end])/(N-1)*2;
    buildDistanceMatrix(pts, max_range);
    bool res = attemptMatch(pts,id_start,id_end, ids, N); 
    std::cout << "old_ids.size():" << ids.size() << std::endl;
    
    std::vector<std::pair<int, int> > vec_conn;
    bool has_problem = checkProblematicConnection(pts, ids, vec_conn);
    while(has_problem && ids.size() != N){
        for( size_t i = 0; i < vec_conn.size(); i++){
            size_t s = vec_conn[i].first;
            size_t t = vec_conn[i].second;
            dist_mat[s][t] = -1;
            dist_mat[t][s] = -1;
        }
        res = attemptMatch(pts, id_start, id_end, ids, N);
        has_problem = checkProblematicConnection(pts, ids, vec_conn);
        std::cout << "ids.size():" << ids.size() << std::endl;
        
        //test
        cv::Mat img(480,640,CV_8UC3);
        img.setTo(cv::Scalar(255,255,255));
        
        for( size_t i = 0; i < ids.size(); i++){
            cv::circle(img, pts[ids[i]],3, cv::Scalar(0,0,255),2,CV_AA);
        }
        for( size_t i = 1; i < ids.size(); i++){
            cv::line(img, pts[ids[i-1]], pts[ids[i]], cv::Scalar(0,255,255),1);
        }
        cv::imshow("img",img);
        cv::waitKey(-1);
    }
    return res;
}
