#include "CircleCalib.h"
#include "Utilities.h"
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>
#include "LAP.h"

#include <opencv2/opencv.hpp>
#include "CornerDetection.h"
#include "PointMatching.h"
#include "CheckerboardCornerClassifier.h"
#include "CalibOptimization.h"

using namespace std;
inline bool loadFeatPts(std::string filePath, std::vector<cv::Point2f>& pts, size_t MGRIDS, size_t NGRIDS){
    ifstream file(filePath.c_str());
    if( !file)
        return false;
    
    string line;
    
    pts.clear();
    int nl = 0;
    while(getline(file, line)){
        std::stringstream ss(line);
        float x, y;
        ss >> x >> y;
        pts.push_back(cv::Point2f(x,y));
        nl++;
    }
        
    if( nl == MGRIDS * NGRIDS)
        return true;
    
    return false;
}

inline bool saveFeatPts(std::string filePath, const std::vector<cv::Point2f>& pts){
    if( pts.empty())
        return false;
    
    ofstream file(filePath.c_str());
    if( !file)
        return false;
    
    for( size_t i = 0; i < pts.size(); i++){
        file << pts[i].x << " " << pts[i].y << std::endl;
    }
    
    return true;
}

CircleCalib::CircleCalib()
{
    switchToCirclePattern(false);
}

bool CircleCalib::listAllImages(string folder){
    _folder_path = folder;
    listImageFiles(_image_paths, folder);
    if( _image_paths.size() > 0){
        _current_index = 0;
        return true;
    }else{
        listImageFiles(_image_paths,folder,".jpg");
        if( _image_paths.size() > 0){
            _current_index = 0;
            return true;
        }
    }
    return false;
}

void CircleCalib::reset(){
    _folder_path = "";
    _image_paths.clear();
    _current_index = -1;
    _current_image = QImage();
    _current_cb_corners.clear();
    clearMarks();
}

void CircleCalib::clearMarks(){
    _bound_corner_ids.clear();
    _cb_corner_order.clear();
    _ponits_by_homo_trans.clear();
    
}

bool CircleCalib::loadMarkRes(int ind){
    if( ind < 0 || ind >= (int) _image_paths.size())
        return false;
    
    string resName = getFilePreName(_image_paths[ind])+".mark";
    
    ifstream file(resName.c_str());
    if( !file)
        return false;
    
    try{
        size_t nblobs;
        file >> nblobs;
        
        _current_cb_corners.resize(nblobs);
        
        for( size_t i = 0; i < _current_cb_corners.size(); i++){
            file >> _current_cb_corners[i].x >> _current_cb_corners[i].y;
        }
        
        _bound_corner_ids.resize(4);
        file >> _bound_corner_ids[0] >> _bound_corner_ids[1] >> _bound_corner_ids[2] >> _bound_corner_ids[3];
        
        int nGrid = MGRIDS*NGRIDS;
        _cb_corner_order.resize((size_t)nGrid);
        for(int i = 0; i < nGrid; i++){
            file >> _cb_corner_order[i];
        }   
    }catch(std::exception& e){
        std::cerr << e.what() << std::endl;
        return false;
    }
    
    return true;
}
bool CircleCalib::saveMarkRes(int ind){
    if( ind < 0 || ind >= (int) _image_paths.size())
        return false;
    
    string resName = getFilePreName(_image_paths[ind])+".mark";
    
    if( _bound_corner_ids.size() == 4 && _cb_corner_order.size() == MGRIDS*NGRIDS){
        ofstream file(resName.c_str());
        if( !file)
            return false;
            
        //points
        file << _current_cb_corners.size() << endl;
        for( size_t i = 0; i < _current_cb_corners.size(); i++){
            file << _current_cb_corners[i].x << " " << _current_cb_corners[i].y << endl;
        }        
        
        //four corners
        file << _bound_corner_ids[0] << " " << _bound_corner_ids[1] << " " << _bound_corner_ids[2] << " " << _bound_corner_ids[3] << endl;
        
        //order
        for( size_t i = 0; i < _cb_corner_order.size(); i++){
            file << _cb_corner_order[i] << endl;
        }
    }
    
    if( _bound_corner_ids.size() == 0){
        QFile::remove(QString::fromStdString(resName));
    }
       
    return true;
}

bool CircleCalib::loadAndDetect(int ind){
    if( ind < 0 || ind >= (int) _image_paths.size())
        return false;
    
    QImage orgImg;
    if( !orgImg.load(QString::fromStdString(_image_paths[_current_index])))
        return false;
    
    _current_image = orgImg;
    _current_cb_corners.clear();
    _bound_corner_ids.clear();
    _cb_corner_order.clear();
    _ponits_by_homo_trans.clear();
    
    if( loadMarkRes(ind))
        return true;
    
    if( !use_checkerboard){
        if(!detectBlobs()){
            switchToCirclePattern(false);
            if( !detectCheckerboardCorners())
                return false;
        }
    }else{
        if( !detectCheckerboardCorners()){
            switchToCirclePattern(true);
            if( !detectBlobs())
                return false;
        }   
    }
    
    /* find the four corners of the bounding quadrangle*/
    if(!detectFourCorners())
        return false;
    
    /* obtain the point ids*/
    if(!establishPointOrder())
        return false;
    
    saveMarkRes(_current_index);
    
    return true;
}

bool CircleCalib::detectBlobs(float threshStep){
    cv::Mat rgb;
    qImage2Mat(_current_image, rgb);
    
    cv::Mat gray;
    cv::cvtColor(rgb,gray, CV_RGB2GRAY);
 
    cv::SimpleBlobDetector::Params params;
    params.filterByConvexity = true;
    params.minConvexity = params.minConvexity*0.9;
    params.minArea = params.minArea*0.5;
    params.thresholdStep = threshStep;
    
    //OpenCV3
     cv::Ptr<cv::SimpleBlobDetector> blobDetector = 
            cv::SimpleBlobDetector::create(params);
    
//cv::Ptr<cv::SimpleBlobDetector> blobDetector = new cv::SimpleBlobDetector(params);
    
    std::vector<cv::KeyPoint> keyPts;
    blobDetector->detect(gray, keyPts);
    
    _current_cb_corners.resize(keyPts.size());
    for(size_t i = 0; i < keyPts.size(); i++){
        _current_cb_corners[i].x = keyPts[i].pt.x;
        _current_cb_corners[i].y = keyPts[i].pt.y;
    }
    return !_current_cb_corners.empty();
}

bool CircleCalib::detectCheckerboardCorners()
{
    cv::Mat rgb;
    qImage2Mat(_current_image, rgb);
    
    cv::Mat gray;
    cv::cvtColor(rgb,gray, CV_RGB2GRAY);
    
    std::vector<cv::Point2f> cur_corners;
    cv::goodFeaturesToTrack(gray, cur_corners, 5000, 0.02, 10, cv::noArray(), 5);
    cv::cornerSubPix(gray, cur_corners,
                     cv::Size(7,7),
                     cv::Size(-1,-1),
                     cv::TermCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS, 20, 0.01));
    
    std::vector<cv::Point2f> cb_corners;
    CheckerboardCornerClassifier ccc;
    ccc.apply(gray,cur_corners, cb_corners);
    
    _current_cb_corners.resize(cb_corners.size());
    for(size_t i = 0; i < cb_corners.size(); i++){
        _current_cb_corners[i].x = cb_corners[i].x;
        _current_cb_corners[i].y = cb_corners[i].y;
    }
    return !_current_cb_corners.empty();
}

bool CircleCalib::detectFourCorners(){
    if( _current_cb_corners.empty())
        return false;
    
    std::vector<int> hull;
    cv::convexHull(_current_cb_corners, hull, false, false);
    
    _bound_corner_ids.clear();
    if( findFourCorners(_current_cb_corners, hull, _bound_corner_ids, MGRIDS, NGRIDS))
        return true;
    return false;
}

bool CircleCalib::establishPointOrderByLAP(){
    _cb_corner_order.clear();
    
    if( _bound_corner_ids.size() != 4)
        return false;
    
    //compute homography first
    vector<cv::Point2f> orgPts;
    vector<cv::Point2f> cornerPts;
    
    orgPts.push_back(cv::Point2f(0,0));
    orgPts.push_back(cv::Point2f(NGRIDS-1,0));
    orgPts.push_back(cv::Point2f(NGRIDS-1,MGRIDS-1));
    orgPts.push_back(cv::Point2f(0,MGRIDS-1));
    
    cornerPts.push_back(_current_cb_corners[_bound_corner_ids[0]]);
    cornerPts.push_back(_current_cb_corners[_bound_corner_ids[1]]);
    cornerPts.push_back(_current_cb_corners[_bound_corner_ids[2]]);
    cornerPts.push_back(_current_cb_corners[_bound_corner_ids[3]]);
    
    cv::Mat H = findHomography(orgPts, cornerPts, 0);
    
    _ponits_by_homo_trans.clear();

	for (int i = 0; i < MGRIDS; i++) {
		for (int j = 0; j < NGRIDS; ++j) {
			float x0 = j;
			float y0 = i;

			float x1 = (float) (H.at<double>(0, 0) * x0
					+ H.at<double>(0, 1) * y0 + H.at<double>(0, 2));
			float y1 = (float) (H.at<double>(1, 0) * x0
					+ H.at<double>(1, 1) * y0 + H.at<double>(1, 2));
			float z1 = (float) (H.at<double>(2, 0) * x0
					+ H.at<double>(2, 1) * y0 + H.at<double>(2, 2));

			x1 /= z1;
			y1 /= z1;

			_ponits_by_homo_trans.push_back(cv::Point2f(x1, y1));
		}
	}
    
    lapMatch(_ponits_by_homo_trans, _current_cb_corners, _cb_corner_order);
    
    /*check if the order is corret*/
    return true;    
}

bool CircleCalib::establishPointOrderByDynamicProgram()
{
   PointMatching pm;
   std::vector<int> left_ids,right_ids;
   pm.matchOneRow(_current_cb_corners, _bound_corner_ids[0], _bound_corner_ids[3], left_ids, MGRIDS);
   pm.matchOneRow(_current_cb_corners, _bound_corner_ids[1], _bound_corner_ids[2], right_ids, MGRIDS);
   
   //test
   cv::Mat img(480,640,CV_8UC3);
   img.setTo(cv::Scalar(255,255,255));
   
   std::vector<int>& ids = right_ids;
   for( size_t i = 0; i < ids.size(); i++){
       cv::circle(img, _current_cb_corners[ids[i]],3, cv::Scalar(0,0,255),2,CV_AA);
   }
   for( size_t i = 1; i < ids.size(); i++){
       cv::line(img, _current_cb_corners[ids[i-1]], _current_cb_corners[ids[i]], cv::Scalar(0,255,255),1);
   }
   cv::imshow("img",img);
   cv::waitKey(-1);
   
   _cb_corner_order.clear();
   for( size_t i = 0; i < MGRIDS; i++){
       std::vector<int> row_order;
       pm.matchOneRow(_current_cb_corners,
                       left_ids[i],
                       right_ids[i],
                       row_order, NGRIDS);
       for( size_t k = 0; k < row_order.size(); k++){
           _cb_corner_order.push_back(row_order[k]);
       }
   }
   
   ids = _cb_corner_order;
   for( size_t i = 0; i < ids.size(); i++){
       cv::circle(img, _current_cb_corners[ids[i]],3, cv::Scalar(0,0,255),2,CV_AA);
   }
   for( size_t i = 1; i < ids.size(); i++){
       cv::line(img, _current_cb_corners[ids[i-1]], _current_cb_corners[ids[i]], cv::Scalar(0,255,255),1);
   }
   cv::imshow("img",img);
   cv::waitKey(-1);
   
   return true;
}

bool CircleCalib::establishPointOrder(){
    if(_bound_corner_ids.size()!=4)
        return false;
    return establishPointOrderByLAP();
    //return establishPointOrderByDynamicProgram();
}

void CircleCalib::lapMatch(const vector<cv::Point2f>& homoPts, 
                      const vector<cv::Point2f>& blobs,
                      vector<int>& inds){
    
    int m = (int) homoPts.size();
    int n = (int) blobs.size();
    
    double* w = new double[m*n];
    
    for( int i = 0; i < m; i++){
        for( int j = 0; j < n; j++){
            double dx = homoPts[i].x - blobs[j].x;
            double dy = homoPts[i].y - blobs[j].y;
            w[i*n+j] = sqrt(dx*dx+dy*dy);
        }
    }
    
    int* x = new int[m];
    int* y = new int[n];
    char* rf = new char[m];
    char* cf = new char[n];
    
    memset(rf, 1, m);
    memset(cf, 1, n);
    lap(w, m, n, x, y, rf, cf);
    
    inds.resize((size_t) m);
    for( int i = 0; i < m; i++){
        inds[i] = x[i];
    }
    
       
    delete[] w;
    delete[] x;
    delete[] y;
    delete[] rf;
    delete[] cf;    
}

bool CircleCalib::canPrev(){
    if( _current_index == 0)
        return false;
    return true;
}

bool CircleCalib::canNext(){
    int maxInd = (int) _image_paths.size() - 1;
    if( _current_index == maxInd)
        return false;
    return true;
}

bool CircleCalib::jumpTo(int ind){
    if( ind < 0 || ind >= (int) _image_paths.size())
        return false;
    _current_index = ind;
    loadAndDetect(_current_index);
    return true;
}

bool CircleCalib::prev(){
    if( canPrev()){
        saveMarkRes(_current_index--);
        loadAndDetect(_current_index);
        return true;
    }
    return false;
}

bool CircleCalib::next(){
    if( canNext()){
        saveMarkRes(_current_index++);
        loadAndDetect(_current_index);
        return true;
    }
    return false;
}

bool CircleCalib::exportFeatPts(){
    saveMarkRes(_current_index);
    int n = 0;
    int oldInd = _current_index;
    for( int ind = 0; ind < (int) _image_paths.size(); ind++){
        if(loadMarkRes(ind)){
            string resPath = getFilePreName(_image_paths[ind])+".txt";
            ofstream file(resPath.c_str());
            if( !file)
                return false;
            for( size_t k = 0; k < _cb_corner_order.size(); k++){
                int i = _cb_corner_order[k];
                file << _current_cb_corners[i].x << " " << _current_cb_corners[i].y << std::endl;
            }
            n++;
        }
    }
    jumpTo(oldInd);
    return n > 0;
}

inline void genObjectPoints(int mgrids, int ngrids, double d, vector<cv::Point3f>& objPts){
    objPts.clear();
    for( int i = 0; i < mgrids; i++){
        for( int j = 0; j < ngrids; j++){
            float x = j*d;
            float y = i*d;
            objPts.push_back(cv::Point3f(x,y,0));
        }
    }
}

bool CircleCalib::doCalibration(double unit,int dist_type){
    saveMarkRes(_current_index);
    
    vector<cv::Point3f> objPts;
    vector<vector<cv::Point3f> > objPtsVec;
    
    genObjectPoints(MGRIDS, NGRIDS, unit, objPts);

    vector<cv::Point2f> imgPts;
    vector<vector<cv::Point2f> > imgPtsVec;
    
    int oldInd = _current_index;
    int nviews = 0;
    for( int ind = 0; ind < (int) _image_paths.size(); ind++){
        if(loadMarkRes(ind)){
            objPtsVec.push_back(objPts);        
            imgPts.clear();
            for( size_t k = 0; k < _cb_corner_order.size(); k++){
                int i = _cb_corner_order[k];
                imgPts.push_back(cv::Point2f(_current_cb_corners[i].x, _current_cb_corners[i].y));
            }         
            imgPtsVec.push_back(imgPts);
            nviews++;
        }
    }   
    //not enough number of views    
    if( nviews <= 1){
        jumpTo(oldInd);
        return false;
    }
    
    K = cv::Mat::zeros(3,3,CV_64F);
    dc = cv::Mat::zeros(1,5,CV_64F);
    
    rvecs.clear();
    tvecs.clear();
    
    //start calibration
    cv::calibrateCamera(objPtsVec,imgPtsVec,
                        cv::Size(_current_image.width(),
                                 _current_image.height()),
                        K, dc, rvecs, tvecs, 0);       
    
    CalibOptimization calib_opt;
    if( dist_type == TypePlumb){
        calib_opt.apply(K,objPtsVec,imgPtsVec,rvecs,tvecs, TypePlumb);
    }
    else if(dist_type == TypePlumb4){
        calib_opt.apply(K,objPtsVec,imgPtsVec,rvecs,tvecs, TypePlumb4);
    }
    else if(dist_type == TypeFOV)
    {
        calib_opt.apply(K,objPtsVec,imgPtsVec,rvecs,tvecs, TypeFOV);
    }else if(dist_type == TypeFisheye){
        calib_opt.apply(K,objPtsVec,imgPtsVec,rvecs,tvecs, TypeFisheye);
    }
    calib_opt.output(K,dc);

    std::cout << K << endl;
    std::cout << dc << endl;
    
    
    jumpTo(oldInd);
    return true;
}

void CircleCalib::removeFeaturePoint(size_t id)
{
    if( id < _current_cb_corners.size()){
        _current_cb_corners.erase(_current_cb_corners.begin()+id);
    }
}
void CircleCalib::addFeaturePoint(float x, float y){
    _current_cb_corners.push_back(cv::Point2f(x, y));
}

CircleCalib& getData(){
    static CircleCalib var;
    return var;
}

int findNearestPoint(const vector<cv::Point2f>& curBlobs, float x, float y){
    int min_i = 0;
    float dist_min = std::numeric_limits<float>::max();
    for( size_t i = 0; i < curBlobs.size(); i++){
        float dx = curBlobs[i].x - x;
        float dy = curBlobs[i].y - y;
        
        float dist = sqrt(dx*dx + dy*dy);
        if( dist < dist_min){
            dist_min = dist;
            min_i = (int) i;
        }
    }    
    return min_i;
}
