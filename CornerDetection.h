#ifndef CORNERDETECTION_H
#define CORNERDETECTION_H

#include "opencv2/opencv.hpp"
#include <vector>

float getInnerAngle(size_t ind, const std::vector<cv::Point2f>& pts);
float getCenterAngle(size_t ind, const std::vector<cv::Point2f>& pts);

bool findFourCorners(const std::vector<cv::Point2f>& pts, 
                     const std::vector<int>& hull,std::vector<int>& corner,
                     size_t MGRIDS, size_t NGRIDS);


#endif // CORNERDETECTION_H
