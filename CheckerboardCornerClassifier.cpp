/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#include "CheckerboardCornerClassifier.h"

CheckerboardCornerClassifier::CheckerboardCornerClassifier(){
    
}

void CheckerboardCornerClassifier::genKer1(cv::Mat &ker,
                                           int hw,
                                           bool is_flip){
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1).clone();
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j= -hw; j <= hw; j++){
            int sign = i*j;
            if( sign > 0)
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( sign < 0)
                ker.at<float>(i+hw,j+hw) = white_value;
            else
                ker.at<float>(i+hw,j+hw) = 0.5f;
        }
    }    
}

void CheckerboardCornerClassifier::genKer2(cv::Mat &ker, 
                                           int hw, 
                                           bool is_flip)
{
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1);
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j= -hw; j <= hw; j++){
            if( abs(i) > abs(j))
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( i == j || i == -j)
                ker.at<float>(i+hw,j+hw) = 0.5f;
            else
                ker.at<float>(i+hw,j+hw) = white_value;
        }
    }
}

void CheckerboardCornerClassifier::genKer3(cv::Mat &ker, int hw, bool is_flip)
{
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1);
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j = -hw; j <= hw; j++){
            if( (i < j && i > 0) ||(i > j && i < 0))
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( i == j )
                ker.at<float>(i+hw,j+hw) = 0.5f;
            else
                ker.at<float>(i+hw,j+hw) = white_value;
        }
    }
}

void CheckerboardCornerClassifier::genKer4(cv::Mat &ker, 
                                           int hw, bool is_flip)
{
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1);
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j = -hw; j <= hw; j++){
            if( (i > j && j > 0) ||(i < j && j < 0))
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( i == j )
                ker.at<float>(i+hw,j+hw) = 0.5f;
            else
                ker.at<float>(i+hw,j+hw) = white_value;
        }
    }
}

void CheckerboardCornerClassifier::genKer5(cv::Mat &ker, int hw, bool is_flip)
{
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1);
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j = -hw; j <= hw; j++){
            if( (i > -j && j < 0) ||(i < -j && j > 0))
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( i == -j )
                ker.at<float>(i+hw,j+hw) = 0.5f;
            else
                ker.at<float>(i+hw,j+hw) = white_value;
        }
    }
}

void CheckerboardCornerClassifier::genKer6(cv::Mat &ker, 
                                           int hw, bool is_flip)
{
    ker = cv::Mat(2*hw+1,2*hw+1,CV_32FC1);
    float black_value = is_flip ? 1.0f:0.0f;
    float white_value = is_flip ? 0.0f:1.0f;
    for( int i = -hw; i <= hw; i++){
        for( int j = -hw; j <= hw; j++){
            if( (i < -j && i > 0) ||(i > -j && i < 0))
                ker.at<float>(i+hw,j+hw) = black_value;
            else if( i == -j )
                ker.at<float>(i+hw,j+hw) = 0.5f;
            else
                ker.at<float>(i+hw,j+hw) = white_value;
        }
    }
}

float CheckerboardCornerClassifier::getZNCC(const cv::Mat &gray, 
                                           const cv::Point2f &corner,
                                           const cv::Mat &ker){
    cv::Mat sub_patch,patch;
    cv::getRectSubPix(gray,cv::Size(ker.cols, ker.rows),corner, sub_patch);
    sub_patch.convertTo(patch, CV_32F);
    float mean_gray = cv::mean(patch)[0];
    patch -= mean_gray;
    float norm_patch = cv::norm(patch);
    float norm_ker = cv::norm(ker);
    return cv::sum(patch.mul(ker))[0]/(norm_patch*norm_ker);
}

void CheckerboardCornerClassifier::apply(const cv::Mat &gray, 
                                         const std::vector<cv::Point2f> &harris_corners, 
                                         std::vector<cv::Point2f> &cb_corners,
                                         int hw,
                                         float thres){
    cb_corners.clear();
    
    cv::Mat ker[6];
    genKer1(ker[0],hw, false);
    genKer2(ker[1],hw, false);
    genKer3(ker[2],hw, false);
    genKer4(ker[3],hw, false);
    genKer5(ker[4],hw, false);
    genKer6(ker[5],hw, false);
    
    for( size_t i = 0; i < harris_corners.size(); i++){
        float zncc_max = 0.0f;
        for( size_t k = 0; k < 6; k++){
            float zncc = fabs(getZNCC(gray,harris_corners[i],ker[k]));
            if( zncc > zncc_max)
                zncc_max = zncc;
        }
        if( zncc_max > thres){
            cb_corners.push_back(harris_corners[i]);
        }
        std::cout << i << "-" <<  zncc_max  << std::endl;
    }
}
