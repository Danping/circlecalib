#ifndef MYDATA_H
#define MYDATA_H

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

#include <QImage>
#include <QPixmap>

#include "CornerDetection.h"

using namespace std;

class CircleCalib
{
public:
    enum{
        TypePlumb = 0,
        TypePlumb4 = 1,
        TypeFisheye = 2,
        TypeFOV = 3
    };
    
    size_t MGRIDS;
    size_t NGRIDS;
    
    bool use_checkerboard;
    
    CircleCalib();
 
    void switchToCirclePattern(bool flag){
        if( flag){
            MGRIDS = 5;
            NGRIDS = 6;
            use_checkerboard = false;
        }else{
            MGRIDS = 8;
            NGRIDS = 6;
            use_checkerboard = true;
        }
    }
    
    /* where the calibration images lies in */
    string _folder_path;    
    /* store the images in the folder*/
    vector<string> _image_paths;
    vector<cv::Point2f> _current_cb_corners;
    /* points transformed by homograpny*/
    vector<cv::Point2f> _ponits_by_homo_trans;
    vector<int> _bound_corner_ids;
    vector<int> _cb_corner_order;
    
    int _current_index;
    QImage _current_image;  //image to be displayed
    
    cv::Mat K,dc;   //intrinsic matrix and distortion coefficients
    std::vector<cv::Mat> rvecs, tvecs;   //rotation and translation vectors
    
    void reset();
    void clearMarks();
    bool listAllImages(string folder);
    
    /* load image and detect corners*/
    bool loadAndDetect(int ind);
    /* detect blobs in the current image*/
    bool detectBlobs(float threshStep = 15.0f);
    /* detect corners in the current image*/
    bool detectCheckerboardCorners();
    bool detectFourCorners();
    bool establishPointOrderByLAP();
    bool establishPointOrderByDynamicProgram();
    bool establishPointOrder();
    void lapMatch(const vector<cv::Point2f>& homoPts, 
                  const vector<cv::Point2f>& blobs,
                  vector<int>& inds);
    
    bool loadMarkRes(int ind);
    bool saveMarkRes(int ind);
    
    bool canPrev();
    bool canNext();
    
    bool jumpTo(int ind);
    bool prev();
    bool next();
    
    bool exportFeatPts();
    bool doCalibration(double unit, int dist_type);
    void removeFeaturePoint(size_t id);
    void addFeaturePoint(float x, float y);
};

CircleCalib& getData();

int findNearestPoint(const vector<cv::Point2f>& curBlobs, float x, float y);

#endif // MYDATA_H
