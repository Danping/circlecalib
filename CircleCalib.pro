#-------------------------------------------------
#
# Project created by QtCreator 2013-11-07T10:46:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CircleDetector
TEMPLATE = app

SOURCES += main.cpp\
        MainWindow.cpp \
    RenderArea.cpp \
    CircleCalib.cpp \
    Utilities.cpp \
    CornerDetection.cpp \
    LAP.cpp \
    DlgShowRes.cpp \
    DlgOption.cpp

HEADERS  += MainWindow.h \
    RenderArea.h \
    MyData.h \
    Utilities.h \
    CornerDetection.h \
    LAP.h \
    DlgShowRes.h \
    DlgOption.h

FORMS    += MainWindow.ui \
    DlgShowRes.ui \
    DlgOption.ui

LIBS += `pkg-config --libs opencv`

OTHER_FILES += \
    Resources/br_prev.png \
    Resources/br_next.png

RESOURCES += \
    CircleCalib.qrc
