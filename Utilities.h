#ifndef UTILITIES_H
#define UTILITIES_H

#include <QImage>
#include <QDir>
#include <QFileInfo>

#include <opencv2/opencv.hpp>

#include <vector>
#include <set>

void mat2QImage(const cv::Mat& mat, QImage& img);
void qImage2Mat(const QImage& img, cv::Mat& mat);

inline void listImageFiles(std::vector<std::string>& imgFiles,
                           const std::string dirPath,
                           const std::string imgext = "*.png"){
           
   QDir folder(QString::fromStdString(dirPath));
   QStringList nameFilter;
   nameFilter << QStringList(QString::fromStdString(imgext));
   QFileInfoList fileList = folder.entryInfoList(nameFilter, QDir::Files);
   
   if(fileList.empty()){
       nameFilter << QStringList(QString::fromStdString("*.jpg"));
       fileList = folder.entryInfoList(nameFilter, QDir::Files);
   }

   imgFiles.clear();
   foreach(QFileInfo f, fileList){
       if( f.isFile()){
           std::string dataPath = std::string(f.absoluteFilePath().toLocal8Bit());
           imgFiles.push_back(dataPath);
       }
   }        
}

inline void splitTokens(std::string buf,std::vector<std::string>& tkList,std::string delimiters)
{
	using namespace std;
	tkList.clear();

	set<char> delimFlag;

	for(size_t i = 0; i < delimiters.size(); i++)
		delimFlag.insert(delimiters[i]);

	size_t i = 0;
	while(i < buf.size()){
		size_t j = i;
		while(j < buf.size()){
			if( delimFlag.count(buf[j]) > 0)
				break;
			j++;
		}
		tkList.push_back(buf.substr(i, j-i));
		i = j+1;
	}
}

inline std::string getFilePreName(std::string filePath)
{
	std::vector<std::string> tkList;
	splitTokens(filePath,tkList,".");
	std::string fileName = tkList.front();
	return fileName;
}

#endif // UTILITIES_H
