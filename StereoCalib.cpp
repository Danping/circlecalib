/**************************************************************
* Author : Danping Zou
* Email  : dpzou@sjtu.edu.cn
* 
* Laboratory of Navigation and Location-based Service
* Shanghai Jiao Tong Unversity
* Notice!! - Distribution is strictly not allowed without the 
* author's permission
**************************************************************/
#include "StereoCalib.h"
#include <string>

using namespace std;
static bool readFeatPts(const string filePath, vector<cv::Point2f>& featPts){
    featPts.clear();
    ifstream file(filePath.c_str());
    if( !file){
        return false;
    }
    
    string line;
    while (!getline(file, line).eof()){
        stringstream ss;
        ss.str(line);
        float x, y;
        ss >> x >> y;
        featPts.push_back(cv::Point2f(x,y));
    }
    return true;
}

template<typename T>
inline double SIGN(T x) {
    return (x >= T(0)) ? +T(1) : -T(1);
}

template<typename T>
inline void mat2quat(const T R[9],
T quat[4]){
    T q0 = (R[0] + R[4] + R[8] + 1.0) / (T(4));
    T q1 = (R[0] - R[4] - R[8] + 1.0) / (T(4));
    T q2 = (-R[0] + R[4] - R[8] + 1.0) /(T(4));
    T q3 = (-R[0] - R[4] + R[8] + 1.0) / (T(4));
    
    if (q0 < 0)
        q0 = 0;
    if (q1 < 0)
        q1 = 0;
    if (q2 < 0)
        q2 = 0;
    if (q3 < 0)
        q3 = 0;
    
    q0 = sqrt(q0);
    q1 = sqrt(q1);
    q2 = sqrt(q2);
    q3 = sqrt(q3);
    
    if (q0 >= q1 && q0 >= q2 && q0 >= q3) {
        q0 *= +1;
        q1 *= SIGN(R[7] - R[5]);
        q2 *= SIGN(R[2] - R[6]);
        q3 *= SIGN(R[3] - R[1]);
    } else if (q1 >= q0 && q1 >= q2 && q1 >= q3) {
        q0 *= SIGN(R[7] - R[5]);
        q1 *= +1;
        q2 *= SIGN(R[3] + R[1]);
        q3 *= SIGN(R[2] + R[6]);
    } else if (q2 >= q0 && q2 >= q1 && q2 >= q3) {
        q0 *= SIGN(R[2] - R[6]);
        q1 *= SIGN(R[3] + R[1]);
        q2 *= +1;
        q3 *= SIGN(R[7] + R[5]);
    } else if (q3 >= q0 && q3 >= q1 && q3 >= q2) {
        q0 *= SIGN(R[3] - R[1]);
        q1 *= SIGN(R[6] + R[2]);
        q2 *= SIGN(R[7] + R[5]);
        q3 *= +1;
    } 
    T r = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
    quat[0] = q0 / r;
    quat[1] = q1 / r;
    quat[2] = q2 / r;
    quat[3] = q3 / r;
}
static void tokenize(const string& str, vector<string>& tokens,
                     const string delim = " ';\t") {
    using namespace std;
    size_t prev = 0, pos;
    tokens.clear();
    while ((pos = str.find_first_of(delim, prev)) != string::npos) {
        if (pos > prev)
            tokens.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < str.length())
        tokens.push_back(str.substr(prev, string::npos));
}

template<typename T>
void readCamParam(const char* fname, T* K, T* kc, int& w, int& h) {
    using namespace std;
    using namespace sl;
    
    ifstream file(fname);
    if (!file)
        throwError("%s - cannot open '%s' to read!", __FUNCTION__, fname);
    
    string str;
    vector<string> tokens;
    int row = 0;
    assert(K && kc);
    while (getline(file, str)) {
        tokenize(str, tokens);
        if (tokens[0][0] == '#') {
            continue; //skip the comments
        }
        if (row < 3) {
            if (tokens.size() < 3)
                throwError("%s - invalid format!", __FUNCTION__);
            for (int j = 0; j < 3; j++) {
                stringstream ss(tokens[j]);
                ss >> K[row * 3 + j];
            }
        } else if (row == 3) {
            if (tokens.size() < 5) {
                throwError("%s - invalid format!", __FUNCTION__);
            }
            for (int j = 0; j < 5; j++) {
                stringstream ss(tokens[j]);
                ss >> kc[j];
            }
        }else if (row == 4){
            if( tokens.size() != 2)
                throwError("%s - invalid format!", __FUNCTION__);
            {
                stringstream ss(tokens[0]);
                ss >> w;
            }
            {
                stringstream ss(tokens[1]);
                ss >> h;
            }
        }
        row++;
    }
    if (row < 3)
        throwError("%s - not complete! - only %d rows read", __FUNCTION__, row);
    
    if (row == 3) {
        fill_n(kc, 5, 0);
        kc[0] = 1;
    }
}


void StereoCalib::doCalibration()
{
    //1.list all left images
    QDir folder(QString::fromStdString(dir));
    QStringList nameFilter;
    nameFilter << QStringList(QString::fromStdString("left*"+imgext));
    QFileInfoList fileList = folder.entryInfoList(nameFilter, QDir::Files);
 
    
    vector<string> leftImages;
    leftImages.clear();
    foreach(QFileInfo f, fileList){
        if( f.isFile()){
            string dataPath = string(f.baseName().toLocal8Bit());
            leftImages.push_back(dataPath);
        }
    } 
    nameFilter.clear();
    nameFilter << QStringList(QString::fromStdString("right*"+imgext));
    fileList = folder.entryInfoList(nameFilter, QDir::Files);
 
    
    vector<string> rightImages;
    rightImages.clear();
    foreach(QFileInfo f, fileList){
        if( f.isFile()){
            string dataPath = string(f.baseName().toLocal8Bit());
            rightImages.push_back(dataPath);
        }
    }
    
    if (leftImages.size() != rightImages.size()){
        sl::throwError("The number of left images should be the same as that of right images!");
    }      
    
    vector<vector<cv::Point3f> > objPtsVec;   
    vector<cv::Point2f> imgPts1,imgPts2;
    vector<vector<cv::Point2f> > imgPts1Vec, imgPts2Vec;
    
    vector<cv::Point3f> objPts;
    for( int i = 0; i < M; i++){
        for( int j = 0;j < N; j++){
            objPts.push_back(cv::Point3f(j*unit, i*unit, 0));
        }
    }
    
    
    size_t nviews = leftImages.size();
    for( size_t i = 0; i < nviews; i++){
        stringstream ss;
        ss << dir << "/" << leftImages[i] << ".txt";
        
        string filePath1 = ss.str();
        
        ss.str("");
        ss << dir << "/" << rightImages[i] << ".txt";
        
        string filePath2 = ss.str();
        
        if(readFeatPts(filePath1, imgPts1) && readFeatPts(filePath2, imgPts2)){
            objPtsVec.push_back(objPts);
            imgPts1Vec.push_back(imgPts1);
            imgPts2Vec.push_back(imgPts2);
        }
    }
    
    float K1[9],K2[9],dc1[5],dc2[5];
    int w, h;
    
    stringstream ss;
    ss << dir << "/cam_cal_left.txt";
    
    readCamParam(ss.str().c_str(),K1,dc1, w, h);
    
    ss.str("");
    ss << dir << "/cam_cal_right.txt";
    readCamParam(ss.str().c_str(),K2,dc2, w, h);
    
    cv::Mat cvK1(3,3,CV_32F, K1);
    cv::Mat cvK2(3,3,CV_32F, K2);
    cv::Mat cvdc1(1,5,CV_32F, dc1);
    cv::Mat cvdc2(1,5,CV_32F, dc2);
    
    cv::Mat R(3,3,CV_32F), T(3,1,CV_32F), E(3,3,CV_32F), F(3,3,CV_32F);
    cv::Size imgSz(w, h);
    
    double err = cv::stereoCalibrate(objPtsVec,imgPts1Vec,imgPts2Vec, cvK1, cvdc1, cvK2, cvdc2, imgSz, R, T, E, F);
    
    cout << "err:" << err << endl;
    cout << R << endl;
    cout << T << endl;
    cout << E << endl;
    cout << F << endl;
    
    //save quaternion + position
    
    double q[4],p[3];
    
    double* pR = (double*) R.data;
    
    p[0] = -(pR[0]*T.at<double>(0,0) + pR[1]*T.at<double>(0,1) + pR[2]*T.at<double>(0,2));
    p[1] = -(pR[3]*T.at<double>(0,0) + pR[4]*T.at<double>(0,1) + pR[5]*T.at<double>(0,2));
    p[2] = -(pR[6]*T.at<double>(0,0) + pR[7]*T.at<double>(0,1) + pR[8]*T.at<double>(0,2));
    
    mat2quat(pR, q);
    q[1] = -q[1];
    q[2] = -q[2];
    q[3] = -q[3];
    
    ss.str("");
    ss << dir << "/stereo_qp_lr.txt";
    ofstream file_qp(ss.str().c_str());
    file_qp.precision(24);
    
    file_qp << q[0] << "\t"
            << q[1] << "\t"
            << q[2] << "\t"
            << q[3] << "\t"
            << p[0] << "\t"
            << p[1] << "\t"
            << p[2];
    
    file_qp.close();    
}
