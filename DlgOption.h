#ifndef DLGOPTION_H
#define DLGOPTION_H

#include <QDialog>

namespace Ui {
class DlgOption;
}

class Option{
public:
    size_t M,N; //grid size
    double unit;
};

class DlgOption : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgOption(Option& opt, QWidget *parent = 0);
    ~DlgOption();
    
    Option& option;
    virtual void accept();
private:
    Ui::DlgOption *ui;
};

#endif // DLGOPTION_H
